/**
 * Created by huhai on 17/6/21.
 */
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  console.log('http request in')
  // res.sendFile(__dirname + '/index.html');
  res.end("welcome to websocket control")
});

var fromBIGscreen = io.of('/fromBIGscreen');
fromBIGscreen.on('connection', function(socket){
  console.log('fromBIGscreen connected');
  socket.emit('message', '你好，来自大屏的访客!');
  fromBIGscreen.emit('message','此消息只能被大屏访客接收！')
});



var fromTOUCHscreen = io.of('/fromTOUCHscreen');
fromTOUCHscreen.on('connection', function(socket){
  console.log('fromBIGscreen connected');
  socket.emit('message', '你好，来自控制屏的访客!');
  socket.on("action", function(msg){

    fromBIGscreen.emit('action',msg)
    socket.emit("message",'已经将指令'+msg+'传递给了大屏')

  })

});

//
// io.on('connection', function(socket){
//   console.log('a user connected');
//   socket.emit('message','this is first message');
//   socket.emit('message', 1, '2', { 3: '4', 5: new Buffer(6) });
//   socket.on('message', function(msg){
//     console.log('message: ' + msg);
//   });
// });



http.listen(3123, function(){
  console.log('listening on *:3123');
});