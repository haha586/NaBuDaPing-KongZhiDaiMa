/**
 * Created by huhai on 17/7/6.
 */
var boot={
  bootSuccess: false,
  init: function(){
    // this.initMessenger();
    this.initWs()
  },
  initMessenger: function(){
    Messenger.options = {
      extraClasses: 'messenger-fixed messenger-on-top',
      theme: 'future',
      showCloseButton: true

    }
    this.show = (msg,failure) => {
      Messenger().post({
        message: msg,
        type: failure ? 'error':'success',
        showCloseButton: true
      });
    }
  },
  initWs: function(){
    // init socket
    this.socket = io('http://121.42.187.170:3123/fromTOUCHscreen');
    this.socket.on('connect', ()=>{
      this.bootSuccess = true
      // this.show("websocket 建立成功!")
      // process the received
      this.socket.emit("message",'huhai ')
    });
    this.socket.on('disconnect', ()=>{
      // this.show('websocket 链接断开!',true)
    });
    this.socket.on('error', ()=>{
      // this.show('websocket error!',true)
    });
    this.socket.on('message',  (data)=> {
      console.log('接收到信息: ' +data)
      // this.show('接收到信息： '+data)
    });
  }
}
boot.init()
